#!/bin/bash


echo $0
cd $(dirname $0)
if [ ! -x "./panki" ]; then
  # get panki
  curl -s "https://gitlab.com/snippets/1847548/raw?inline=false" -o panki
  chmod +x panki
fi
rm -rf html
panki . html
